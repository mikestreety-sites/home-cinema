# Home Cinema

3 random films from our list, showing only the synopsis and stats first, with the option to reveal the film.

A bit more info (and the why) can be found [on my blog](https://www.mikestreety.co.uk/blog/love-film-and-cinema/).
